import avatar from "./assets/images/48.jpg";

function App() {
  return (
    <div>
      <div>
        <img src={avatar} alt="avatar" />
      </div>
      <div>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div>
        <p>
          <b>Tammy Stevens</b> * Front End Developer
        </p>
      </div>
    </div>
  );
}

export default App;
